<div class="content-wrapper">
<section class="content-header" style="padding: 0px 0px 0 0px;">
		<div class="row">
			<div class="col-xs-12">
				<div class="box" style="">
          			<div class="box-header">
						<p style="font-size:28px; padding: 10px 0px 0 10px;">
							Data Menu Snack
						</p>
						<ol class="breadcrumb" style="background-color:#ffffff; margin: -10px 0px 10px -1px;">
							<li><a href="#">Beranda</a></li>
							<li><a href="#">Snack</a></li>
						</ol>
						<!-- <div class="visible-md visible-lg" style="float:right; margin: -65px 10px -90px 10px;">
							<button type="button" class="button btn-success button4" data-toggle="modal" data-target="#modal-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Data</button>
						</div> -->
					</div>
				</div>
			</div>
		</div>
  	</section>
	<section class="content">
		<div class="box"><br>
			<span style="margin: 30px; font-size: 20px; font-family: robooto;">Menu Snack Terfavorit</span>
			<hr>
			<div style="width: 100%; background-color: #fff; border-radius: 3px; box-shadow: 0 1px 2px #c8d1d3;">
				<div style="padding: 30px; margin-top: -20px;">
					<div class="row">
						<div class="col-xs-12">
							<div class="row">
								<?php foreach($fav as $data) : ?>
									<div class="col-md-3 product-container" style="padding-left: 15px; padding-right: 15px; width: 25%;">
										<div class="thumbnail">
										<img class="product-image" style="height:200px; width:90%;" src="<?php echo base_url('images/'.$data->gambar); ?>">
											<div class="product-detail" style="padding: 10px; text-align: center;">
												<div style="border-bottom: 1px dashed #ddd; height: 46px; overflow: hidden; font-weight: bold;">
													<span><?php echo ucwords(strtolower($data->nama_menu)); ?></span>
												</div>
												<div style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px dashed #ddd; font-size: 13px;">
													<span>
													<?php echo ucwords(strtolower("Rasa: <span class='red-color'> $data->keterangan </span>&nbsp;&nbsp;")) . "<br>";?>
													</span>
												</div>
												<div style="padding-top: 5px; font-size: 14px; padding-bottom: 5px; margin-bottom: 5px; ">
													<span class="red-color">
														<?php echo ucwords(strtolower("Harga: Rp <span class='red-color'> $data->harga </span>&nbsp;&nbsp;")) ;?>
													</span>
													&nbsp;&nbsp;&nbsp;
													<span>Stock :&nbsp; <strong class="product-stock"><?php echo $data->stok_menu; ?></strong></span>
												</div>
												<?php 
												$ada = 'Ada';
												if ($data->stok_menu == $ada) { ?>
													<a style="height:25px;" onclick="addCart('<?php echo $data->id_menu;?>')"
													class="btn btn-xs btn-block btn-success add-product-trigger"
													id="<?php echo $data->id_menu; ?>">
														<strong>Add to Cart</strong>
													</a>
												<?php } else { ?>
													<a onclick="stockEmpty()"
													class="btn btn-xs btn-block btn-default add-product-trigger"
													id="<?php echo $data->id_menu; ?>">
														<strong>Stock Kosong</strong>
													</a>
												<?php } ?>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    	<div class="box"><br>
			<span style="margin: 30px; font-size: 20px; font-family: robooto;">Daftar Menu Snack</span>
			<hr>
			<div style="width: 100%; background-color: #fff; border-radius: 3px; box-shadow: 0 1px 2px #c8d1d3;">
				<div style="padding: 30px; margin-top: -20px;">
					<div class="row">
						<div class="col-xs-12">
							<div class="row">
								<?php foreach($images as $data) : ?>
									<div class="col-md-3 product-container" style="padding-left: 15px; padding-right: 15px; width: 25%;">
										<div class="thumbnail">
										<img class="product-image" style="height:200px; width:90%;" src="<?php echo base_url('images/'.$data->gambar); ?>">
											<div class="product-detail" style="padding: 10px; text-align: center;">
												<div style="border-bottom: 1px dashed #ddd; height: 46px; overflow: hidden; font-weight: bold;">
													<span><?php echo ucwords(strtolower($data->nama_menu)); ?></span>
												</div>
												<div style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px dashed #ddd; font-size: 13px;">
													<span>
													<?php echo ucwords(strtolower("Rasa: <span class='red-color'> $data->keterangan </span>&nbsp;&nbsp;")) . "<br>";?>
													</span>
												</div>
												<div style="padding-top: 5px; font-size: 14px; padding-bottom: 5px; margin-bottom: 5px; ">
													<span class="red-color">
														<?php echo ucwords(strtolower("Harga: Rp <span class='red-color'> $data->harga </span>&nbsp;&nbsp;")) ;?>
													</span>
													&nbsp;&nbsp;&nbsp;
													<span>Stock :&nbsp; <strong class="product-stock"><?php echo $data->stok_menu; ?></strong></span>
												</div>
												<?php 
												$ada = 'Ada';
												if ($data->stok_menu == $ada) { ?>
													<a style="height:25px;" onclick="addCart('<?php echo $data->id_menu;?>')"
													class="btn btn-xs btn-block btn-success add-product-trigger"
													id="<?php echo $data->id_menu; ?>">
														<strong>Add to Cart</strong>
													</a>
												<?php } else { ?>
													<a onclick="stockEmpty()"
													class="btn btn-xs btn-block btn-default add-product-trigger"
													id="<?php echo $data->id_menu; ?>">
														<strong>Stock Kosong</strong>
													</a>
												<?php } ?>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
