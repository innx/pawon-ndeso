<style>
.alert {
    padding: 20px;
    background-color: #4CAF50;
    color: white;
}

.closebtn {
    margin-left: 15px;
    color: white;
    font-weight: bold;
    float: right;
    font-size: 22px;
    line-height: 20px;
    cursor: pointer;
    transition: 0.3s;
}

.closebtn:hover {
    color: black;
}
</style>
<div class="content-wrapper">
  	<section class="content-header" style="padding: 0px 0px 0 0px;">
		<div class="row">
			<div class="col-xs-12">
				<div class="box" style="">
          			<div class="box-header">
						<p style="font-size:28px; padding: 10px 0px 0 10px;">
							Data Admin
						</p>
						<ol class="breadcrumb" style="background-color:#ffffff; margin: -10px 0px 10px -10px;">
							<li><a href="#">Beranda</a></li>
							<li><a href="#">Akun</a></li>
							<li><a href="#">Data Admin</a></li>
						</ol>
					</div>
				</div>
			</div>
		</div>
  	</section>
  	<section class="content">
    	<div class="row">
			<div class="col-xs-12">
				<div class="box">
          			<div class="box-header">
            			<h3 class="box-title">Data Admin</h3>
						<div class="visible-md visible-xs" style="float:right; margin: 15px -1px 1px 10px;">
							<button type="button" class="button btn-success button4" data-toggle="modal" data-target="#modal-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Data</button>
						</div>
          			</div>
					<?php if(validation_errors() || $this->session->flashdata('message_akun')){ ?>
						<div class="alert alert-success animated fadeInDown eula1" role="alert" >
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<?php echo $this->session->flashdata('message_akun'); ?>
						</div>
					<?php } ?>
          			<div class="box-body">
						<div class="row">
							<div class="col-sm-12">
								<table id="example" class="table table-striped table-bordered">
									<thead>
										<tr role="row">
											<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 3%;">No</th>
											<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 20%; height:30%;">Gambar</th>
											<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 20%;">Nama</th>
											<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 15%;">Username</th>
											<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 15%;">Email</th>
											<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 20%;">Telepon</th>
											<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 20%;">Status</th>
											<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 20%;">Level</th>
											<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 8%;">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php $no = $this->uri->segment('3') + 1; ?>
										<?php foreach($level as $data) : ?>
										<tr role="row" class="even">
											<td><center><?=$no++;?></center></td>
											<td> <img src="<?php echo base_url('images/'.$data->gambar); ?>" alt="" style="height=70%; width:100%;"/></td>
											<td><?php echo $data->name; ?></td>
											<td><?php echo $data->username; ?></td>
											<td><?php echo $data->email; ?></td>
											<td><?php echo $data->telepon; ?></td>
											<td>
												<center>
												<?php if($data->status == 1){ ?>
													<span class='label label-success'>Aktif</span>
												<?php } else { ?>
													<span class='label label-danger'>Non-Aktif</span>
												<?php } ?>
												</center>
											</td>
											<td>
												<center>
												<?php if($data->level == 1){ ?>
													<span class='label label-success'>Super Admin</span>
												<?php } else { ?>
													<span class='label label-primary'>Admin</span>
												<?php } ?>
												</center>
											</td>
											<td>
												<a href="<?php echo base_url('administrator/edit/'.$data->id_admin); ?>">
													<i class="fa fa-pencil" title="Edit" style="color:#99CC00; float:left; margin-left:10px;"></i>
												</a>
												<?php
													$hapus = array(
														'class' => 'fa fa-trash',
												     	'title' => 'Hapus',
														'style' => 'color:#99CC00; float:right; margin-right:10px;',
														'onclick' => 'javascript: return confirm(\'Anda yakin menghapus akun ' . $data->name . '?\')'
													);
													echo anchor(site_url('administrator/hapus/' . $data->id_admin), ' ', $hapus);
												?>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
