<aside class="main-sidebar">
  <section class="sidebar">
  	<!-- search form -->
    <!-- <form action="#" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
					<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
					</button>
				</span>
			</div>
    </form> -->
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li>
        <a href="<?php echo base_url("admin") ?>">
          <i class="fa fa-home"></i> <span>Beranda</span>
        </a>
      </li>
			<li class="treeview">
          <a href="#">
            <i class="fa fa-list"></i>
            <span>Data Menu</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li><a href="<?php echo base_url("admin/makanan") ?>"><i class="fa fa-circle-o"></i> Makanan</a></li>
            <li><a href="<?php echo base_url("admin/minuman") ?>"><i class="fa fa-circle-o"></i> Minuman</a></li>
            <li><a href="<?php echo base_url("admin/snack") ?>"><i class="fa fa-circle-o"></i> Snack</a></li>
          </ul>
        </li>
			<li>
        <a href="<?php echo base_url("admin/order") ?>">
				<i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>Order Baru</span>
          <span class="pull-right-container">
            <small class="label pull-right bg-green">new</small>
          </span>
        </a>
			</li>
			<li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Akun</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url("admin/akun") ?>"><i class="fa fa-circle-o"></i> Daftar</a></li>
            <li><a href="<?php echo base_url("admin/profil") ?>"><i class="fa fa-circle-o"></i> Profile</a></li>
						<?php $level=$this->session->userdata('level');
						 if($level == 1){
							echo "<li><a href=".base_url('administrator')."><i class='fa fa-circle-o'></i> Data Admin</a></li>";
						}?>
          </ul>
				</li>
				<li>
        <a href="<?php echo base_url("admin/logout") ?>">
          <i class="fa fa-sign-out"></i> <span>Logout</span>
        </a>
      </li>
    </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
