<div class="page">
  <div class="container">
    <div class="left">
			<center>
				<div class="gambar">
					<img src="asset/image/pawonndeso.png">
				</div>
			</center>
			<?php if(validation_errors() || $this->session->flashdata('result_login')){ ?>
				<div class="alert alert-danger animated fadeInDown eula1" role="alert" >
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Peringatan!</strong>
					<?php echo validation_errors(); ?>
					<?php echo $this->session->flashdata('result_login'); ?>
				</div>
				<center style="margin-top: 20%;">
					<strong>Create By &copy; 2018 <a href="#" style="color:#FF0000;">Yudha</a></strong>
				</center> 
			<?php }else{ ?>
      	<div class="eula">By logging in you agree to the ridiculously long terms that you didn't bother to read</div>
				<center style="margin-top: 30%;">
					<strong>Create By &copy; 2018 <a href="#" style="color:#FF0000;">Yudha</a></strong>
				</center> 
			<?php } ?>
		</div>
    <div class="right">
      <svg viewBox="0 0 320 300">
        <defs>
          <linearGradient
            inkscape:collect="always"
            id="linearGradient"
            x1="13"
            y1="193.49992"
            x2="307"
            y2="193.49992"
            gradientUnits="userSpaceOnUse">
          <stop
            style="stop-color:#ff00ff;"
            offset="0"
            id="stop876" />
          <stop
            style="stop-color:#ff0000;"
            offset="1"
            id="stop878" />
        </linearGradient>
      </defs>
      <path d="m 40,120.00016 239.99984,-3.2e-4 c 0,0 24.99263,0.79932 25.00016,35.00016 0.008,34.20084 -25.00016,35 -25.00016,35 h -239.99984 c 0,-0.0205 -25,4.01348 -25,38.5 0,34.48652 25,38.5 25,38.5 h 215 c 0,0 20,-0.99604 20,-25 0,-24.00396 -20,-25 -20,-25 h -190 c 0,0 -20,1.71033 -20,25 0,24.00396 20,25 20,25 h 168.57143" />
    </svg>
		<form action="<?php echo base_url('Login/proses'); ?>" method="post" >
      <div class="form">
        <label for="username">Username</label>
        <input type="text" id="username" name="username" autofocus="true" required="">
        <label for="password">Password</label>
        <input type="password" id="password" name="password" required="">
        <input type="submit" id="submit" value="Login">
      </div>
		</form>
    </div>
  </div>
</div>
<script>
var current = null;
document.querySelector('#username').addEventListener('focus', function(e) {
  if (current) current.pause();
  current = anime({
    targets: 'path',
    strokeDashoffset: {
      value: 0,
      duration: 700,
      easing: 'easeOutQuart'
    },
    strokeDasharray: {
      value: '240 1386',
      duration: 700,
      easing: 'easeOutQuart'
    }
  });
});
document.querySelector('#password').addEventListener('focus', function(e) {
  if (current) current.pause();
  current = anime({
    targets: 'path',
    strokeDashoffset: {
      value: -336,
      duration: 700,
      easing: 'easeOutQuart'
    },
    strokeDasharray: {
      value: '240 1386',
      duration: 700,
      easing: 'easeOutQuart'
    }
  });
});
document.querySelector('#submit').addEventListener('focus', function(e) {
  if (current) current.pause();
  current = anime({
    targets: 'path',
    strokeDashoffset: {
      value: -730,
      duration: 700,
      easing: 'easeOutQuart'
    },
    strokeDasharray: {
      value: '530 1386',
      duration: 700,
      easing: 'easeOutQuart'
    }
  });
});
</script>
<script src="<?php echo base_url('asset/new/js/anime.min.js')?>"></script>
<script src="<?php echo base_url('asset/image/js/jquery-2.1.4.min.js')?>"></script>
<script src="<?php echo base_url('asset/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
</body>
</html>
