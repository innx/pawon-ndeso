<?php 
class Order extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('m_data');
		$this->load->helper('url');

	}

	function index(){
		$data['data'] = $this->session->userdata('cart');
		$this->load->view('pesan/html/html_open');
		$this->load->view('pesan/html/header');
		$this->load->view('pesan/html/aside');
		$this->load->view('pesan/order',$data);
		$this->load->view('pesan/html/footer');
	}
}
