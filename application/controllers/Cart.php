<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller
{
    /**
     * Submit Cart
     * for Logged in User
     */
    public function add()
    {
        // Set Cart
        $stockId = $this->input->post('stockId');
        $cart = $this->session->userdata('cart');
        if(isset($stockId)  && $stockId != "") {
            if (count($cart)) {
                if (in_array($stockId, $cart)) {
                    echo 0;
                    exit();
                } else {
                    array_push($cart, $stockId);
                    $this->session->set_userdata('cart', $cart);
                    print_r($cart);
                    exit();
                }
            } else {
                $cart = array();
                array_push($cart, $stockId);
                $this->session->set_userdata('cart', $cart);
                print_r($cart);
                exit();
            }
		}
		print_r($this->input->post('stockId'));
		print_r('<hr>');
    }


    /**
     * Submit Cart
     * for Logged in User
     */
    public function edit()
    {
        // Set Cart
        $stockId = $this->input->post('stockId');
        $orderId = (string)$this->input->post('orderId').'-cart';
        $cart = $this->session->userdata($orderId);

        if(isset($stockId)) {
            if (count($cart)) {
                if (in_array($stockId, $cart)) {
                    echo 0;
                    exit();
                } else {
                    array_push($cart, $stockId);
                    $this->session->set_userdata($orderId, $cart);
                    print_r($cart);
                    echo $orderId;
                    exit();
                }
            } else {
                $cart = array();
                array_push($cart, $stockId);
                $this->session->set_userdata($orderId, $cart);
                print_r($cart);
                echo $orderId;
                exit();
            }
        }

    }


}
