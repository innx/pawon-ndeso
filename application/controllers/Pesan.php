<?php 
class Pesan extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('m_data');
		$this->load->helper('url');

	}

	function index(){
		$this->load->database();
		$jumlah_data = $this->m_data->jumlah_makanan();
		$this->load->library('pagination');
		$config['base_url'] = base_url().'pesan/index';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 8;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);	
		$data['images'] = $this->m_data->data($config['per_page'],$from);
	
		$data['fav'] = $this->m_data->tampil_data_makanan_favorit();
		$this->load->view('pesan/html/html_open');
		$this->load->view('pesan/html/header');
		$this->load->view('pesan/html/aside');
		$this->load->view('pesan/makanan',$data);
		$this->load->view('pesan/html/footer');
	}

	function minuman(){
		$this->load->database();;
		$data['images']=$this->m_data->tampil_data_minuman_favorit_umum();
		$data['fav'] = $this->m_data->tampil_data_minuman_favorit();
		$this->load->view('admin/html/html_open');
		$this->load->view('admin/html/header');
		$this->load->view('pesan/html/aside');
		$this->load->view('pesan/minuman',$data);
		$this->load->view('admin/html/footer');
	}

	function snack(){
		$this->load->database();;
		$data['images']=$this->m_data->tampil_data_snack_favorit_umum();
		$data['fav'] = $this->m_data->tampil_data_snack_favorit();
		$this->load->view('admin/html/html_open');
		$this->load->view('admin/html/header');
		$this->load->view('pesan/html/aside');
		$this->load->view('pesan/snack',$data);
		$this->load->view('admin/html/footer');
	}
}
