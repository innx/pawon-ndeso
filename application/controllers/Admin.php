<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('m_data');
		$this->load->library(array('form_validation', 'session'));
		// $this->load->model(array('Mod_Login'));
		if (!isset($this->session->userdata['id_admin'])) {
			redirect(base_url("Login"));
		}
	}

	public function index()
	{
		$this->load->view('admin/html/html_open');
		$this->load->view('admin/html/header');
		$this->load->view('admin/html/aside');
		$this->load->view('admin/admin');
		$this->load->view('admin/html/footer');
	}

	public function order()
	{
		$this->load->view('admin/html/html_open');
		$this->load->view('admin/html/header');
		$this->load->view('admin/html/aside');
		$this->load->view('admin/orders');
		$this->load->view('admin/html/footer');
	}

	public function makanan()
	{   
		$this->load->database();
		$data['kategori']=$this->m_data->kategori();
		$data['favorit']=$this->m_data->favorit();
		$data['images'] = $this->m_data->tampil_data_makanan();
		$this->load->view('admin/html/html_open');
		$this->load->view('admin/html/header');
		$this->load->view('admin/html/aside');
		$this->load->view('admin/makanan',$data);
		$this->load->view('admin/html/footer');
	}

	public function minuman()
	{
		$this->load->database();
		$data['kategori']=$this->m_data->kategori();
		$data['favorit']=$this->m_data->favorit();
		$data['images'] = $this->m_data->tampil_data_minuman();
		$this->load->view('admin/html/html_open');
		$this->load->view('admin/html/header');
		$this->load->view('admin/html/aside');
		$this->load->view('admin/minuman',$data);
		$this->load->view('admin/html/footer');
	}

	public function snack()
	{
		$this->load->database();
		$data['kategori']=$this->m_data->kategori();
		$data['favorit']=$this->m_data->favorit();
		$data['images'] = $this->m_data->tampil_data_snack();
		$this->load->view('admin/html/html_open');
		$this->load->view('admin/html/header');
		$this->load->view('admin/html/aside');
		$this->load->view('admin/snack',$data);
		$this->load->view('admin/html/footer');
	}

	public function akun()
	{
		$this->load->database();
		$this->load->view('admin/html/html_open');
		$this->load->view('admin/html/header');
		$this->load->view('admin/html/aside');
		$this->load->view('admin/profile/akun');
		$this->load->view('admin/html/footer');
	}
	public function profil()
	{
		$this->load->view('admin/html/html_open');
		$this->load->view('admin/html/header');
		$this->load->view('admin/html/aside');
		$this->load->view('admin/profile/profil');
		$this->load->view('admin/html/footer');
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('Login'));
	}
}
?>
