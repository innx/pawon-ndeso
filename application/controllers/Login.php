<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  function __construct() {
    parent::__construct();
    if ($this->session->userdata('username')) {
      redirect(base_url("admin"));
    }else
    $this->load->model(array('Mod_Login'));
  }

  function index() {
    $valid = $this->form_validation;
    $this->load->view("login/metta");
    $this->load->view('login/login');
    $valid->set_rules('username','Username','required');
    $valid->set_rules('password','Password','required');
    if($valid->run()) {
        $this->simple_login->login($username,$password, base_url('admin'), base_url('login'));
    }
  }
	function proses()
  {
    $username   = strtolower($this->input->post('username'));
    $password   = md5($this->input->post('password'));
    $result     = $this->Mod_Login->auth($username, $password);
    if ($result) {
      if ($result[0]['status'] == 1) {
          $sess = array(
							'id_admin'  => $result[0]['id_admin'],
							'name'      => $result[0]['name'],
							'username'  => $result[0]['username'],
							'email'		  => $result[0]['email'],
							'telepon'   => $result[0]['telepon'],
							'status'    => $result[0]['status'],
							'level'    => $result[0]['level'],
							'gambar'    => $result[0]['gambar'],
              'logged_in' => TRUE
          );
          $this->session->set_userdata($sess);
          redirect(base_url('admin'));
        
      } else {
        $this->session->set_flashdata('result_login', 'Username Anda '.ucwords($username).' Sedang Dinonaktifkan');
        redirect(base_url('login'));
      }
    } else {
      $this->session->set_flashdata('result_login', 'Kombinasi Username atau Password Salah');
      redirect(base_url('login'));
    }
	}
}
?>
