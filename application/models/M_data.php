<?php 

class M_data extends CI_Model{

	function __construct()
	{
	  parent::__construct();
		  $this->load->database();
		  $this->db->select('*');    
		  $this->db->from('tb_menu');
		  $this->db->join('tb_kategori', 'tb_menu.id_kategori = tb_kategori.id_kategori');
		  $this->db->join('tb_favorit','tb_favorit.id_favorit=tb_kategori.id_kategori');
		  $query = $this->db->get();
	  return $query->result();
	}

	function get_by_id($id)
	{
	  return $this->db->get_where('tb_menu',array('id_menu'=>$id))->row();
	}

	function tampil_data_profil(){
	  return $this->db->get_where('tb_admin',array('id_admin'=>$id))->row();
	}
	function tampil_data_admin(){
		$this->db->order_by('id_admin DESC');
		return $this->db->get('tb_admin')->result();
	}

	function tampil_data_makanan_favorit(){
		$this->db->order_by('id_menu DESC');
		return $this->db->get_where('tb_menu',array('id_favorit'=>1,'id_kategori'=>1))->result();
	}

	function data($number,$offset){
		
		$this->db->order_by('id_menu DESC');
		return $query = $this->db->get_where('tb_menu', array('id_kategori'=>1), $number,$offset)->result();		
	}
	
	function jumlah_makanan(){
		$this->db->order_by('id_menu DESC');
		return $this->db->get_where('tb_menu', array('id_favorit'=>4,'id_kategori'=>1))->num_rows();
	}

	function tampil_data_makanan_favorit_umum($number,$offset){
		$this->db->order_by('id_menu DESC');
		return $this->db->get_where('tb_menu',$number,$offset, array('id_favorit'=>4,'id_kategori'=>1))->result();
	}

	function tampil_data_makanan(){
		$this->db->order_by('id_menu DESC');
		return $this->db->get_where('tb_menu',array('id_kategori'=>1))->result();
	}
	
	function tampil_data_minuman_favorit(){
		$this->db->order_by('id_menu DESC');
		return $this->db->get_where('tb_menu',array('id_favorit'=>2,'id_kategori'=>2))->result();
	}

	function tampil_data_minuman_favorit_umum(){
		$this->db->order_by('id_menu DESC');
		return $this->db->get_where('tb_menu',array('id_favorit'=>4,'id_kategori'=>2))->result();
	}

	function tampil_data_minuman(){
		$this->db->order_by('id_menu DESC');
		return $this->db->get_where('tb_menu',array('id_kategori'=>2))->result();
	}

	function tampil_data_snack_favorit(){
		$this->db->order_by('id_menu DESC');
		return $this->db->get_where('tb_menu',array('id_favorit'=>3,'id_kategori'=>3))->result();
	}

	function tampil_data_snack_favorit_umum(){
		$this->db->order_by('id_menu DESC');
		return $this->db->get_where('tb_menu',array('id_favorit'=>4,'id_kategori'=>3))->result();
	}

	function tampil_data_snack(){
		$this->db->order_by('id_menu DESC');
		return $this->db->get_where('tb_menu',array('id_kategori'=>3))->result();
	}

function kategori(){
		$query = $this->db->get('tb_kategori');
		return $query->result_array();
	}

	function favorit(){
		$query = $this->db->get('tb_favorit');
		return $query->result_array();
	}

	function insert($data)
	{
	  $this->db->insert('tb_menu', $data);
	}
 
	function update($data,$where){
		$this->db->update('tb_menu',$data,$where);
	}

	function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	function daftar($data)
	{
	  $this->db->insert('tb_admin', $data);
	}

	function jumlah_data(){
		return $this->db->get_where('tb_menu',array('id_kategori'=>1))->num_rows();
	}
}
