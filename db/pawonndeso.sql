-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 10 Des 2018 pada 16.15
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pawonndeso`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` int(10) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `username` varchar(10) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `gambar` varchar(200) DEFAULT NULL,
  `status` int(5) DEFAULT NULL,
  `level` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `name`, `username`, `email`, `password`, `telepon`, `gambar`, `status`, `level`) VALUES
(65, 'Pawon Ndeso', 'admin', 'pawonndeso@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', '085200300400', 'Hydrangeas.jpg', 1, 1),
(66, 'Pawon Ndeso', 'pawonndeso', 'email@pawon.com', '5f4dcc3b5aa765d61d8327deb882cf99', '085400300200', 'Jellyfish.jpg', 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_favorit`
--

CREATE TABLE `tb_favorit` (
  `id_favorit` int(30) NOT NULL,
  `nama_favorit` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_favorit`
--

INSERT INTO `tb_favorit` (`id_favorit`, `nama_favorit`) VALUES
(1, 'makanan favorit'),
(2, 'minuman favorit'),
(3, 'snack favorit'),
(4, 'umum');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id_kategori` int(30) NOT NULL,
  `nama_kategori` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kategori`
--

INSERT INTO `tb_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Makanan'),
(2, 'Minuman'),
(3, 'Snack');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_laporan`
--

CREATE TABLE `tb_laporan` (
  `id_laporan` int(30) NOT NULL,
  `id_order` int(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_menu`
--

CREATE TABLE `tb_menu` (
  `id_menu` int(30) NOT NULL,
  `id_kategori` int(30) DEFAULT NULL,
  `id_favorit` int(30) DEFAULT NULL,
  `nama_menu` varchar(30) DEFAULT NULL,
  `harga` int(10) DEFAULT NULL,
  `stok_menu` varchar(30) DEFAULT NULL,
  `keterangan` varchar(30) DEFAULT NULL,
  `keterangan2` varchar(30) NOT NULL,
  `keterangan3` varchar(30) NOT NULL,
  `keterangan4` varchar(30) NOT NULL,
  `gambar` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_menu`
--

INSERT INTO `tb_menu` (`id_menu`, `id_kategori`, `id_favorit`, `nama_menu`, `harga`, `stok_menu`, `keterangan`, `keterangan2`, `keterangan3`, `keterangan4`, `gambar`) VALUES
(2, 1, 1, 'Ayam Geprek', 15000, 'Ada', 'mercon', '', '', '', '8f0d6b5248ac4de6eab3932b7b2ee7ae-8fd9f35d2f2d83c2afdcf77932ccd0bf_600x4001.jpg'),
(3, 1, 1, 'Gurami Bakar', 34000, 'Ada', 'mercon', '', '', '', 'gurami.JPG'),
(4, 1, 1, 'Capcay Kuah Seafood', 12000, 'Ada', 'asam pedas', '', '', '', 'images_mancanegara_capcay_capcay-kuah-seafood.jpg'),
(5, 1, 4, 'Nasi Goreng', 18000, 'Ada', 'Pawon Ndeso', '', '', '', 'namaNasi_Goreng.jpg'),
(6, 1, 4, 'Tumis Kangkung', 12000, 'Ada', 'mercon', '', '', '', 'Tumis_Kangkung.jpg'),
(7, 1, 1, 'Mie Aceh', 15000, 'Ada', 'mercon', '', '', '', 'Mie_Aceh.png'),
(8, 1, 4, 'Ayam Asam Manis', 15000, 'Ada', 'asam pedas', '', '', '', 'Ayam_Asam_Manis.jpeg'),
(9, 1, 4, 'Chicken Fire Wings', 20000, 'Ada', 'mercon', '', '', '', 'Chicken_Fire_Wings.jpg'),
(11, 1, 4, 'Sop Ayam', 15000, 'Ada', 'kuah gurih', '', '', '', 'Sop_Ayam1.jpeg'),
(12, 1, 4, 'Salem Balado', 10000, 'Ada', 'mercon', '', '', '', 'Salem_Balado.jpg'),
(13, 1, 4, 'Kepiting Asam Manis', 25000, 'Ada', 'mercon', '', '', '', 'Kepiting_Asam_Manis.jpg'),
(14, 1, 4, 'Nila Asam Manis', 15000, 'Habis', 'asam pedas', '', '', '', 'Nila_Asam_Manis.jpg'),
(15, 1, 4, 'Chicken Fillet', 15000, 'Habis', 'mercon', '', '', '', 'Chicken_Fillet.jpg'),
(16, 1, 4, 'Gurame Asam Manis', 32000, 'Ada', 'mercon', '', '', '', 'Gurame_Asam_Manis.jpg'),
(17, 1, 4, 'Kwetiau Kuah Pedas', 12000, 'Ada', 'mercon', '', '', '', 'Kwetiau_Kuah_Pedas.jpg'),
(18, 2, 4, 'Orange', 6000, 'Ada', 'segar', '', '', '', '4d490cb15c61db86afa03f620606221a.jpg'),
(19, 2, 4, 'Cappuccino Ice', 12000, 'Ada', 'segar', '', '', '', '83CappucinoBlend.jpg'),
(20, 2, 4, 'Lemon Tea', 7000, 'Habis', 'segar', '', '', '', '1280-463541757-iced-tea-and-lemon.jpg'),
(21, 2, 4, 'Jus Alpukat', 12000, 'Ada', 'segar', '', '', '', '218636751_8f4e79fe-bd42-4c43-bfda-6e1d7b8a0973_950_1430.jpg'),
(22, 2, 4, 'Bubble Ice', 10000, 'Ada', 'segar', '', '', '', 'cara-membuat-bubble-drink.jpg'),
(23, 2, 4, 'Coffe', 8000, 'Ada', 'segar', '', '', '', 'diffcoffee1.jpg'),
(24, 2, 3, 'Es Teller', 15000, 'Ada', 'segar', '', '', '', 'es-teler_20170611_142527.jpg'),
(25, 2, 4, 'MilkShake Chocolate', 15000, 'Ada', 'segar', '', '', '', 'hqdefault.jpg'),
(27, 2, 4, 'Air Mineral', 4000, 'Ada', 'segar', '', '', '', 'Le_Minerale_Air_Mineral_Botol_24_X_330_ML_grande.jpg'),
(29, 2, 4, 'MilkShake Oreo', 12000, 'Ada', 'segar', '', '', '', 'Oreo-Milkshake1.jpg'),
(30, 2, 4, 'Ice Aloevera', 11000, 'Ada', 'segar', '', '', '', 'photo.jpg'),
(31, 2, 4, 'Ice Cream Chocolate', 10000, 'Ada', 'manis', '', '', '', 'Resep-Es-Krim-Gelato-Cokelat-Nan-Lezat.jpg'),
(32, 2, 4, 'Teh Hangat', 7000, 'Ada', 'manis', '', '', '', 'large-teh-hangat1.jpg'),
(33, 2, 2, 'King Mango', 12000, 'Ada', 'segar', '', '', '', 'yuk-membuat-king-mango-thai-resep-jus-mangga-kekinian-mudah-dan-sehat.jpg'),
(34, 2, 2, 'Blue ocean', 15000, 'Ada', 'segar', '', '', '', '4_air_minuman_Blue_Ocean_sunquick_dan_soda_terbaik_puasa.jpg'),
(36, 1, 4, 'Lele Penyet', 12000, 'Habis', 'Mercon', 'Penyet', '-', '-', 'Lele_Penyet1.jpg'),
(38, 1, 4, 'Ayam Bakar', 12000, 'Ada', 'Mercon', 'Madu', 'Asap', '-', 'ayam_bakar1.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_order`
--

CREATE TABLE `tb_order` (
  `id_order` int(30) NOT NULL,
  `id_user` int(30) DEFAULT NULL,
  `id_menu` int(11) NOT NULL,
  `total` int(30) DEFAULT NULL,
  `bayar` int(30) DEFAULT NULL,
  `tanggal` int(20) DEFAULT NULL,
  `no_transaksi` int(30) DEFAULT NULL,
  `jam` time(6) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `no_meja` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_orderdetail`
--

CREATE TABLE `tb_orderdetail` (
  `id_orderdetail` int(30) NOT NULL,
  `id_order` int(30) DEFAULT NULL,
  `id_menu` int(30) DEFAULT NULL,
  `jumlah` int(30) DEFAULT NULL,
  `total_harga` int(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(10) NOT NULL,
  `nama_user` varchar(20) DEFAULT NULL,
  `alamat_user` varchar(20) DEFAULT NULL,
  `no_telp` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tb_favorit`
--
ALTER TABLE `tb_favorit`
  ADD PRIMARY KEY (`id_favorit`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tb_laporan`
--
ALTER TABLE `tb_laporan`
  ADD PRIMARY KEY (`id_laporan`);

--
-- Indexes for table `tb_menu`
--
ALTER TABLE `tb_menu`
  ADD PRIMARY KEY (`id_menu`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_favorit` (`id_favorit`);

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_orderdetail`
--
ALTER TABLE `tb_orderdetail`
  ADD PRIMARY KEY (`id_orderdetail`),
  ADD KEY `id_menu` (`id_menu`),
  ADD KEY `id_order` (`id_order`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `tb_favorit`
--
ALTER TABLE `tb_favorit`
  MODIFY `id_favorit` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id_kategori` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_laporan`
--
ALTER TABLE `tb_laporan`
  MODIFY `id_laporan` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_menu`
--
ALTER TABLE `tb_menu`
  MODIFY `id_menu` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `id_order` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_orderdetail`
--
ALTER TABLE `tb_orderdetail`
  MODIFY `id_orderdetail` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_menu`
--
ALTER TABLE `tb_menu`
  ADD CONSTRAINT `tb_menu_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `tb_kategori` (`id_kategori`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_menu_ibfk_2` FOREIGN KEY (`id_favorit`) REFERENCES `tb_favorit` (`id_favorit`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_order`
--
ALTER TABLE `tb_order`
  ADD CONSTRAINT `tb_order_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_orderdetail`
--
ALTER TABLE `tb_orderdetail`
  ADD CONSTRAINT `tb_orderdetail_ibfk_1` FOREIGN KEY (`id_menu`) REFERENCES `tb_menu` (`id_menu`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_orderdetail_ibfk_2` FOREIGN KEY (`id_order`) REFERENCES `tb_order` (`id_order`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
